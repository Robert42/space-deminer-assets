#version 120

varying vec4 oUv;
varying vec4 oColor;
varying float zPosition;

void main()
{
  if(zPosition == -1)
  {
    gl_FragColor = oColor;
  }else if(zPosition >= 0)
  {
    vec4 frontColor = vec4(oColor.rrra);
    vec4 backColor = vec4(oColor.rrr, oColor.a*oColor.b);
    float pixelWidth = oColor.g;
    
    float tile = mod(oUv.x+oUv.y+0.001, pixelWidth);
    float factor;
    float antialiasing = zPosition;
    
    if(antialiasing>0)
    {
      if(tile>pixelWidth*0.7)
        factor = smoothstep(pixelWidth-antialiasing, pixelWidth, tile);
      else
        factor = smoothstep(pixelWidth*0.5, pixelWidth*0.5-antialiasing, tile);
    }else
    {
      factor = step(pixelWidth*0.5, tile);
    }
  
    gl_FragColor = mix(frontColor, backColor, factor);
  }
}
