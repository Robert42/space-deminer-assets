#if GLSL_VERSION==130
#version 130
#define USE_NAN_CHECK
#else
#version 120

#endif

uniform sampler2D atlas;
varying vec4 oUv;
varying vec4 oColor;
varying float zPosition;

ivec2 mod_(ivec2 v, int a)
{
  return v-a*(v/a);
}

vec4 premultiply(vec4 color)
{
  return vec4(color.rgb*color.a, color.a);
}

vec4 unpremultiply(vec4 color)
{
  if(color.a!=0)
    return vec4(color.rgb/color.a, color.a);
  else
    return color;
}

float mean(vec3 v)
{
  return (v.x+v.y+v.z) / 3.0;
}


vec4 applyRange(vec4 color, vec2 range)
{
  return vec4((color.rgb-range.x)*range.y, color.a);
}


vec3 falseColorRamp[7] = vec3[7](vec3(0.0, 0.0, 0.0),
                                 vec3(0.5, 0.0, 1.0),
                                 vec3(1.0, 0.0, 1.0),
                                 vec3(1.0, 0.0, 0.0),
                                 vec3(1.0, 0.5, 0.0),
                                 vec3(1.0, 1.0, 0.0),
                                 vec3(1.0, 1.0, 1.0));

vec4 falseColor(vec4 color)
{
  float value = mean(premultiply(color).xyz);
  
  float darkUp = clamp(1 - (1-value)*(1-value), 0, 1);
  
  int colorA = int(clamp(floor(value * 6), 0, 6));
  int colorB = int(clamp(ceil(value * 6), 0, 6));
  float weight = mod(value * 6, 1.0);
  
  return vec4(mix(falseColorRamp[colorA], falseColorRamp[colorB], weight) * darkUp, 1);
}

const int DISPLAY_MODE_NORMAL = 0;
const int DISPLAY_MODE_IGNORE_ALPHA = DISPLAY_MODE_NORMAL + 1;
const int DISPLAY_MODE_RED_CHANNEL = DISPLAY_MODE_IGNORE_ALPHA + 1;
const int DISPLAY_MODE_GREEN_CHANNEL = DISPLAY_MODE_RED_CHANNEL + 1;
const int DISPLAY_MODE_BLUE_CHANNEL = DISPLAY_MODE_GREEN_CHANNEL + 1;
const int DISPLAY_MODE_ALPHA_CHANNEL = DISPLAY_MODE_BLUE_CHANNEL + 1;

const int DISPLAY_FILTER_NONE = 0;
const int DISPLAY_FILTER_PREMULTIPLY = DISPLAY_FILTER_NONE+1;
const int DISPLAY_FILTER_UNPREMULTIPLY = DISPLAY_FILTER_PREMULTIPLY+1;
const int DISPLAY_FILTER_FALSE_COLOR = DISPLAY_FILTER_UNPREMULTIPLY+1;

void main()
{
  bool isBackground = zPosition>0.f;
  bool isHighlight = zPosition<0.f;

  if(isBackground)
  {
    ivec2 tile = ivec2(oUv.xy+0.01)/8;
    
    tile = mod_(tile, 2);
    
    bool useLightTile = (tile.x == tile.y);
    
    vec4 lightTileColor = oColor.rrra;
    vec4 darkTileColor = oColor.ggga;

    gl_FragColor = useLightTile ? lightTileColor : darkTileColor;
  }else if(isHighlight)
  {
    gl_FragColor = vec4(0);
    vec4 secondColor = vec4(0);
    
    vec4 texColor = texture2D(atlas, oUv.xy);
    ivec2 parametersToCheckFor = ivec2(oColor.xy);
    vec2 range = oColor.zw;
    
    bool checkForNan = false;
    bool checkForInf = false;
    bool checkForTooSmall = false;
    bool checkForTooLarge = false;
    
    if(parametersToCheckFor.x == 3)
    {
      checkForNan = true;
      checkForInf = true;
    }else if(parametersToCheckFor.x == 2)
    {
      checkForInf = true;
    }else if(parametersToCheckFor.x == 1)
    {
      checkForNan = true;
    }
    
    if(parametersToCheckFor.y == 3)
    {
      checkForTooSmall = true;
      checkForTooLarge = true;
    }else if(parametersToCheckFor.y == 2)
    {
      checkForTooLarge = true;
    }else if(parametersToCheckFor.y == 1)
    {
      checkForTooSmall = true;
    }
    
    
    vec4 colorWithCorrectedRange = applyRange(texColor, range);
    
    
    if(checkForTooSmall && any(lessThan(colorWithCorrectedRange, vec4(0))))
    {
      gl_FragColor = vec4(1, 1, 1, 1);
      secondColor = vec4(0, 0, 0, 1);
    }
    if(checkForTooLarge && any(greaterThan(colorWithCorrectedRange, vec4(1))))
    {
      gl_FragColor = vec4(0, 0, 0, 1);
      secondColor = vec4(1, 1, 1, 1);
    }
    
#ifdef USE_NAN_CHECK
    if(checkForInf && any(isinf(texColor)))
    {
      gl_FragColor = vec4(0, 1, 1, 1);
      secondColor = vec4(0, 0, 0, 1);
    }
    if(checkForNan && any(isnan(texColor)))
    {
      gl_FragColor = vec4(1, 0, 0, 1);
      secondColor = vec4(0, 0, 0, 1);
    }
#endif

    bool boolUseSecondColor = (zPosition==-2.f);
    
    if(boolUseSecondColor)
      gl_FragColor = secondColor;
  }else
  {
    vec4 texColor = texture2D(atlas, oUv.xy);
    
    int displayMode = int(oColor.x);
    int displayFilter = int(oColor.y);
    vec2 range = oColor.zw;
    
    
    if(displayMode == DISPLAY_MODE_IGNORE_ALPHA)
      texColor = vec4(texColor.rgb, 1.0);
    else if(displayMode == DISPLAY_MODE_RED_CHANNEL)
      texColor = vec4(texColor.rrr, 1.0);
    else if(displayMode == DISPLAY_MODE_GREEN_CHANNEL)
      texColor = vec4(texColor.ggg, 1.0);
    else if(displayMode == DISPLAY_MODE_BLUE_CHANNEL)
      texColor = vec4(texColor.bbb, 1.0);
    else if(displayMode == DISPLAY_MODE_ALPHA_CHANNEL)
      texColor = vec4(texColor.aaa, 1.0);
      
    texColor = applyRange(texColor, range);
    
    if(displayFilter == DISPLAY_FILTER_PREMULTIPLY)
      texColor = premultiply(texColor);
    else if(displayFilter == DISPLAY_FILTER_UNPREMULTIPLY)
      texColor = unpremultiply(texColor);
    else if(displayFilter == DISPLAY_FILTER_FALSE_COLOR)
      texColor = falseColor(texColor);
    gl_FragColor = texColor;
  }
}
