# Space-Deminer Assets

This project contains the assets for Space-Deminer. Make sure you are using the same branch.

## License

All content you can see here is licensed by the Creative Common License (CC-BY):

- This work is licensed under the Creative Commons Attribution 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
- The TweakUI cursors in framework/developer-hud/tweak-ui-cursors.png are based on the cursors provided withe the source of [AntTweakBar](http://anttweakbar.sourceforge.net/doc/tools:anttweakbar)
