#version 120

attribute vec4 uv0;

varying vec2 UV;

void main()
{
  gl_Position = ftransform();
  UV = vec2(uv0);
}
