#version 120

varying vec2 UV;

uniform sampler2D colormap;
uniform sampler2D starfield;

void main()
{
  vec4 starLayers = texture2D(starfield, UV) * texture2D(colormap, UV);
  float starBrightness = min(1.0, starLayers.r + starLayers.g + starLayers.b + starLayers.a);

  gl_FragColor = starBrightness * vec4(1, 1, 1, 0) + vec4(0, 0, 0, 1);
}
