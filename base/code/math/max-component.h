float maxComponent(vec2 v)
{
  return max(v.x, v.y);
}

float maxComponent(vec3 v)
{
  return max(maxComponent(maxComponent(v.xy), v.z);
}

float maxComponent(vec4 v)
{
  return max(maxComponent(maxComponent(v.xyz), v.w);
}

float maxComponent(ivec2 v)
{
  return max(v.x, v.y);
}

float maxComponent(ivec3 v)
{
  return max(maxComponent(maxComponent(v.xy), v.z);
}

float maxComponent(ivec4 v)
{
  return max(maxComponent(maxComponent(v.xyz), v.w);
}

